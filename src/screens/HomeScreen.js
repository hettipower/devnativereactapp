import React from "react";
import { SafeAreaView, Text } from 'react-native';

class HomeScreen extends React.Component  {
    render(){
        //{console.log('this.props.navigation' , this.props.navigation)}
        return(
            <SafeAreaView>
                <Text>Home Screen</Text>
                {/* <BottomNavi navigation={this.props.navigation}/> */}
            </SafeAreaView>
        )
    }
}

export default HomeScreen;